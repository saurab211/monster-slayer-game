const app = Vue.createApp({

    data () {
        return {
            monsterHealth: 100,
            playerHealth: 100,
            roundCounter: 0,
            winner: null,
            logMessages: [],
        };
    },

    // to relatively make less code logic at html use compuuted properties
    computed: {
        monsterBarStyle() {

            if (this.monsterHealth < 0) {
                return {width: '0%'};
            }
            return  {width: this.monsterHealth + '%' }; 

        },

        playerBarStyle() {
            if (this.playerHealth < 0) {
                return {width: '0%'};
            }

            return { width: this.playerHealth + '%' }; 

        },
        specialAttackRule () {
            return this.roundCounter % 3 !== 0; 
        }

    },

   

    methods: {

        logMessageRecord (who, what, how) {
            this.logMessages.unshift({
                attacker: who,
                attackType: what,
                attackValue: how,
            });

        },

        startGame () {
            this.monsterHealth = 100;
            this.playerHealth = 100;
            this.roundCounter = 0;
            this.winner = null; 
            this.logMessages = [];
        },
        surrender () {
            this.winner = 'monster';
        },

        healPlayer () {
            this.roundCounter++;
            const healValue = Math.floor(Math.random() * 18) + 10;
            if (this.playerHealth + healValue > 100) {
                this.playerHealth = 100;
            } else {
                this.playerHealth += healValue;
            }
            this.attackPlayer(); 
            this.logMessageRecord('Player', 'Heal', healValue); 

        },
        attackMonster () {
            this.roundCounter++; 
            const attackValue = Math.floor(Math.random() * 12) + 5;
            this.monsterHealth -= attackValue;
            this.attackPlayer(); 
            this.logMessageRecord('Player', 'Attack', attackValue); 

        },

        attackPlayer() {
            const attackValue = Math.floor(Math.random() * 18) + 8;
            this.playerHealth -= attackValue;
            this.logMessageRecord('Monster', 'Attack', attackValue); 


        },

        specialAttack () {
            this.roundCounter++; 
            const attackValue = Math.floor(Math.random() * 25) + 12;
            this.monsterHealth -= attackValue;
            this.attackPlayer(); 
            this.logMessageRecord('Player', 'Special - Attack', attackValue); 


        }


    },
    watch: {
        playerHealth (value) {
            if (value <= 0 && this.monsterHealth <= 0) {
                // draw
                this.winner = 'draw';
            } else if (value <= 0) {
                // player lost
                this.winner = 'monster';
            }
        },

        monsterHealth (value) {
            if (value <= 0 && this.playerHealth <= 0) {
                // draw
                this.winner = 'draw';
            } else if (value <= 0) {
                // monster lost
                this.winner = 'player';
            }

        }


    },






});

app.mount('#game-container'); 